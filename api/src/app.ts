import express from "express";
 import cep from 'cep-promise';
 import cors from 'cors';
 
//  require('dotenv').config();

//  const nodemailer = require('nodemailer');

//  // Passo 1

//  let transporter = nodemailer.createTransport({
//      service: 'smtp.gmail.com',
//      auth: {
//          user: 'TestandoApi.2020@gmail.com',
//          pass: 'TestandoApi20201',
//      }
     
//  });


// // Passo 2
//  let mailOptions = {
//      from: 'TestandoApi.2020@gmail.com',
//      to: 'jessicasilva.jesusOfc@gmail.com',
//      subject: ' Testing and Testing',
//      text: ' IT works'

//  };

//  // Passo 3

//  transporter.sendMail(mailOptions, function(err:any, data:any) {
//      if (err){
//          console.log('Erro',err);
//      }else{
//          console.log('Email enviado!!!');
//      }

//  });



 const app = express();
 
 app.use(cors())
 
 
 app.get('/cep', async (req, res) => {
     const cepParam = req.query.value;
     const cepReturned = await cep(`${cepParam}`)
     res.send(cepReturned);
 })
 
 const port = 3001;
 app.listen(port, () => {
     console.log(`running at port: ${port}`);
 });
